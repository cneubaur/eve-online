using System;
using System.Linq;

namespace EveOnline.Client
{
    public static class Extensions
    {
        public static T GetCustomAttribute<T>(this Type type) where T : Attribute
        {
            return type.GetCustomAttributes(inherit: false).First(x => x is T) as T;
        }
    }
}