using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace EveOnline.Client
{
    public class CharacterClient : ClientBase
    {
        private int? _characterId { get; set; }

        public CharacterClient(HttpClient httpClient, ILogger<CharacterClient> logger) : base(httpClient, logger)
        {
        }

        // public async Task<SolarSystem> GetLocation()
        // {
        //     await Verify();

        //     var path = $"latest/characters/{_characterId}/location";
        //     var response = await _httpClient.GetAsync(path);

        //     response.EnsureSuccessStatusCode();

        //     var body = await response.Content.ReadAsStringAsync();

        //     var location = JsonConvert.DeserializeAnonymousType(body, new { solar_system_id = 0 });

        //     return _staticData.Regions.SelectMany(x => x.SolarSystems).First(x => x.Id == location.solar_system_id);
        // }

        private async Task Verify()
        {
            if (_characterId != null)
                return;

            var response = await _httpClient.GetAsync("verify");
            response.EnsureSuccessStatusCode();
            var body = await response.Content.ReadAsStringAsync();

            var charInfo = JsonConvert.DeserializeAnonymousType(body, new { CharacterID = 0 });

            _characterId = charInfo.CharacterID;
        }
    }
}