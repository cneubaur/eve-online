using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace EveOnline.Client
{
    public class MarketClient : ClientBase
    {
        private readonly UniverseClient _universeClient;

        public MarketClient(HttpClient httpClient, UniverseClient universeClient, ILogger<UniverseClient> logger) : base(httpClient, logger)
        {
            _universeClient = universeClient;
        }

        public Task<IEnumerable<MarketOrder>> GetOrders(ulong regionId, ulong typeId, string orderType = "all")
        {
            return Request<IEnumerable<MarketOrder>>($"markets/{regionId}/orders?type_id={typeId}&order_type={orderType}");
        }

        public async Task<IEnumerable<MarketHistory>> GetHistory(ulong regionId, ulong typeId)
        {
            var history = await Request<IEnumerable<MarketHistory>>($"markets/{regionId}/history?type_id={typeId}");
            return history.OrderByDescending(x => x.Date);
        }
    }
}