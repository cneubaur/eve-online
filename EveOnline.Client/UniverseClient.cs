using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace EveOnline.Client
{
    public class UniverseClient : ClientBase
    {
        private readonly ConcurrentDictionary<Type, ConcurrentDictionary<ulong, object>> _itemCache = new ConcurrentDictionary<Type, ConcurrentDictionary<ulong, object>>();
        private readonly ConcurrentDictionary<Type, IEnumerable<ulong>> _listCache = new ConcurrentDictionary<Type, IEnumerable<ulong>>();

        public UniverseClient(HttpClient httpClient, ILogger<UniverseClient> logger) : base(httpClient, logger)
        {
        }

        public async Task<T> Get<T>(ulong id)
        {
            if (_itemCache.TryGetValue(typeof(T), out var cache))
            {
                if (cache.TryGetValue(id, out var existingItem))
                    return (T)existingItem;
            }
            else
            {
                cache = new ConcurrentDictionary<ulong, object>();
                _itemCache[typeof(T)] = cache;
            }

            var urlPathAttr = typeof(T).GetCustomAttribute<UrlPathAttribute>();
            var item = await Request<T>(urlPathAttr.Path + "/" + id.ToString());

            cache[id] = item;
            return item;
        }

        public async Task<IEnumerable<ulong>> List<T>()
        {
            if (_listCache.TryGetValue(typeof(T), out var cache))
                return cache;

            var urlPathAttr = typeof(T).GetCustomAttribute<UrlPathAttribute>();
            var itemIds = await Request<IEnumerable<ulong>>(urlPathAttr.Path);

            _listCache[typeof(T)] = itemIds;

            return itemIds;
        }

        public async Task<IEnumerable<ulong>> Search(string search, string category, bool strict = true)
        {
            var response = await _httpClient.GetAsync($"search?search={search}&categories={category}&strict={strict}");
            response.EnsureSuccessStatusCode();

            var body = await response.Content.ReadAsStringAsync();
            var ids = JsonConvert.DeserializeObject<Dictionary<string, IEnumerable<ulong>>>(body);

            return ids[category];
        }

        public Task<IEnumerable<ulong>> Search<T>(string search, bool strict = true)
        {
            var eveCatAttr = typeof(T).GetCustomAttribute<EveCategoryAttribute>();
            return Search(search, eveCatAttr.Name, strict);
        }

        public async Task<IEnumerable<NameInfo>> GetNameInfo<T>(IEnumerable<string> names)
        {
            var eveCatAttr = typeof(T).GetCustomAttribute<EveCategoryAttribute>();
            var nameInfos = await Post<Dictionary<string, IEnumerable<NameInfo>>>("universe/ids", names);
            return nameInfos[eveCatAttr.PluralName];
        }

        public Task<IEnumerable<NameInfo>> GetNameInfo(IEnumerable<ulong> ids)
        {
            return Post<IEnumerable<NameInfo>>("universe/names", ids);
        }

        public Task<IEnumerable<ulong>> GetRoute(ulong originId, ulong destinationId)
        {
            return Request<IEnumerable<ulong>>($"route/{originId}/{destinationId}");
        }
    }
}