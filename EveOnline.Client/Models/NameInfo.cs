using Newtonsoft.Json;

namespace EveOnline.Client
{
    [UrlPath("universe/names")]
    public class NameInfo
    {
        [JsonProperty("id")]
        public ulong Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("category")]
        public string CategoryName { get; set; }
    }
}