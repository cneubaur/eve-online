using Newtonsoft.Json;

namespace EveOnline.Client
{
    [EveCategory("region", "regions")]
    [UrlPath("universe/regions")]
    public class Region
    {
        [JsonProperty("region_id")]
        public ulong Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}