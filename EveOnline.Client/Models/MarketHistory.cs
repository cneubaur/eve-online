using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EveOnline.Client
{
    public class MarketHistory
    {
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("order_count")]
        public ulong OrderCount { get; set; }
        
        [JsonProperty("lowest")]
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public double LowestPrice { get; set; }
        
        [JsonProperty("highest")]
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public double HighestPrice { get; set; }
        
        [JsonProperty("average")]
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public double AveragePrice { get; set; }

        [JsonProperty("volume")]
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public float Volume { get; set; }
    }
}