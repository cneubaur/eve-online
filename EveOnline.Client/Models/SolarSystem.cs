using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EveOnline.Client
{
    [EveCategory("solar_system", "solar_systems")]
    [UrlPath("universe/systems")]
    public class SolarSystem
    {
        [JsonProperty("system_id")]
        public ulong Id { get; set; }

        [JsonProperty("security_status")]
        [DisplayFormat(DataFormatString = "{0:N1}")]
        public double SecurityStatus { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("constellation_id")]
        public int ConstellationId { get; set; }
    }
}