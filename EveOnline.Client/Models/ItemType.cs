using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EveOnline.Client
{
    [EveCategory("inventory_type", "inventory_types")]
    [UrlPath("universe/types")]
    public class ItemType
    {
        [DisplayName("ID")]
        [JsonProperty("type_id")]
        public ulong Id { get; set; }

        [DisplayName("Name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [DisplayName("Volume")]
        [JsonProperty("volume")]
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public float Volume { get; set; }
    }
}