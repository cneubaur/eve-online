using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace EveOnline.Client
{
    public class MarketOrder
    {
        [JsonProperty("type_id")]
        public ulong ItemTypeId { get; set; }

        [JsonProperty("is_buy_order")]
        public bool IsBuyOrder { get; set; }

        [JsonProperty("price")]
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public double Price { get; set; }

        [JsonProperty("system_id")]
        public ulong SolarSystemId { get; set; }

        [JsonProperty("location_id")]
        public ulong LocationId { get; set; }
        
        [JsonProperty("min_volume")]
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public ulong MinVolume { get; set; }

        [JsonProperty("volume_total")]
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public ulong VolumeTotal { get; set; }
        
        [JsonProperty("volume_remain")]
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public ulong VolumeRemain { get; set; }
        
        [JsonProperty("range")]
        public string Range { get; set; }
    }
}