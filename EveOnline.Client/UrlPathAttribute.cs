using System;

namespace EveOnline.Client
{
    public class UrlPathAttribute : Attribute
    {
        public string Path { get; set; }

        public UrlPathAttribute(string path)
        {
            Path = path;
        }
    }
}