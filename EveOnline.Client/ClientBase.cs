using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace EveOnline.Client
{
    public class ClientBase
    {
        protected readonly HttpClient _httpClient;
        protected readonly ILogger _logger;

        public ClientBase(HttpClient httpClient, ILogger logger)
        {
            _httpClient = httpClient;
            _logger = logger;
        }

        public async Task<T> Request<T>(string path)
        {
            HttpResponseMessage response;

            for (var i = 0; i < 5; i++)
            {
                response = await _httpClient.GetAsync(path);

                if (response.StatusCode == HttpStatusCode.BadGateway)
                {
                    _logger.LogWarning("Unable to contact ESI API. Retry after 100ms");
                    await Task.Delay(100);
                    continue;
                }

                response.EnsureSuccessStatusCode();
                var body = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(body);
            }

            throw new HttpRequestException("Unable to contact ESI API");
        }

        public async Task<T> Post<T>(string path, object data)
        {
            var json = JsonConvert.SerializeObject(data);
            var content = new StringContent(json);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await _httpClient.PostAsync(path, content);
            var body = await response.Content.ReadAsStringAsync();
            
            response.EnsureSuccessStatusCode();
            return JsonConvert.DeserializeObject<T>(body);
        }
    }
}