using System;

namespace EveOnline.Client
{
    public class EveCategoryAttribute : Attribute
    {
        public string Name { get; set; }
        public string PluralName { get; set; }

        public EveCategoryAttribute(string name, string pluralName)
        {
            Name = name;
            PluralName = pluralName;
        }
    }
}