using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using EveOnline.Client;

namespace NebularMining.Core
{
    public class MarketStats
    {
        [DisplayName("Item")]
        public ItemType ItemType { get; set; }

        [DisplayName("Average Price")]
        [DisplayFormat(DataFormatString = "{0:N2}", NullDisplayText = "-")]
        public double AveragePrice { get; set; }

        [DisplayName("Average Sold Volume")]
        [DisplayFormat(DataFormatString = "{0:N2}")]
        public float AverageSoldVolume { get; set; }
        
        [DisplayName("Average Sold Units")]
        [DisplayFormat(DataFormatString = "{0:N0}")]
        public int AverageSoldUnits { get; set; }
        
        [DisplayName("Best Order")]
        public MarketOrderInfo BestOrder { get; set; }

        [DisplayName("Prive Difference")]
        [DisplayFormat(DataFormatString = "{0:N2}", NullDisplayText = "-")]
        public double PriceDifference => BestOrder is null ? 1.0 : BestOrder.Price / AveragePrice;
    }
}