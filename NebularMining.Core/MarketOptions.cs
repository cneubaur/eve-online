namespace NebularMining.Core
{
    public class MarketOptions
    {
        public double BaseBrokerFee { get; set; } = 0.05;
        public double TransactionTax { get; set; } = 0.05;
    }
}