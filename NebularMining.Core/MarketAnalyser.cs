using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using EveOnline.Client;
using Microsoft.Extensions.Options;

namespace NebularMining.Core
{
    public class MarketAnalyzer
    {
        private readonly UniverseClient _universeClient;
        private readonly MarketClient _marketClient;
        private readonly MarketOptions _options;

        public MarketAnalyzer(UniverseClient universeClient, MarketClient marketClient, IOptions<MarketOptions> options)
        {
            _universeClient = universeClient;
            _marketClient = marketClient;
            _options = options.Value;
        }

        public async Task<IEnumerable<MarketStats>> GetStats(ulong regionId, IEnumerable<string> itemList, double securityStatus = 0.7, int days = 7)
        {
            var itemTypeNameInfos = await _universeClient.GetNameInfo<ItemType>(itemList);
            var getItemTypeTasks = itemTypeNameInfos.Select(nameInfo => _universeClient.Get<ItemType>(nameInfo.Id));

            var getMarketStatsTasks = getItemTypeTasks.Select(async getItemTypeTask => {
                var itemType = await getItemTypeTask;
                var history = await _marketClient.GetHistory(regionId, itemType.Id);
                var orders = await _marketClient.GetOrders(regionId, itemType.Id);
                
                var averageSoldUnits = (int)history.Take(days).Select(x => x.Volume).Average();
                var averagePrice = history.Take(days).Select(x => x.AveragePrice).Average();

                var getSolarSystemTasks = orders.Select(x => _universeClient.Get<SolarSystem>(x.SolarSystemId));
                var solarSystems = await Task.WhenAll(getSolarSystemTasks);

                var marketOrderInfos = orders
                    .Zip(solarSystems, (order, solarSystem) => 
                    {
                        var estimatedRevenueGross = order.Price * averageSoldUnits;

                        return new MarketOrderInfo
                        {
                            IsBuyOrder = order.IsBuyOrder,
                            Price = order.Price,
                            NormedPrice = order.Price / itemType.Volume,
                            SolarSystem = solarSystem,
                            BrokerFee = order.IsBuyOrder ? 0.0 : estimatedRevenueGross * _options.BaseBrokerFee,
                            TransactionTax = estimatedRevenueGross * _options.TransactionTax,
                            EstimatedRevenueGross = estimatedRevenueGross,
                        };
                    })
                    .Where(x => x.SolarSystem.SecurityStatus >= securityStatus);

                if (marketOrderInfos.Count() == 0)
                    return new MarketStats
                    {
                        ItemType = itemType,
                        AveragePrice = averagePrice,
                        AverageSoldVolume = averageSoldUnits * itemType.Volume,
                        AverageSoldUnits = averageSoldUnits,
                    };

                return new MarketStats {
                    ItemType = itemType,
                    AveragePrice = averagePrice,
                    AverageSoldVolume = averageSoldUnits * itemType.Volume,
                    AverageSoldUnits = averageSoldUnits,
                    BestOrder = GetBestOrder(marketOrderInfos),
                };
            });

            var marketStats = await Task.WhenAll(getMarketStatsTasks);

            return marketStats.OrderByDescending(x => x.BestOrder?.EstimatedRevenueNet);
        }

        private MarketOrderInfo GetBestOrder(IEnumerable<MarketOrderInfo> marketOrders)
        {
            var ordered = marketOrders.OrderBy(x => x.NormedPrice);

            // Get order with lowest price for sell orders
            var bestSellOrder = ordered.Where(x => !x.IsBuyOrder).FirstOrDefault();

            // Get order with highest price for buy orders
            var bestBuyOrder = ordered.Where(x => x.IsBuyOrder).LastOrDefault();

            return bestSellOrder?.EstimatedRevenueNet > bestBuyOrder?.EstimatedRevenueNet ? bestSellOrder : bestBuyOrder;
        }
    }
}
