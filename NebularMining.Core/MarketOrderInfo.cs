using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using EveOnline.Client;

namespace NebularMining.Core
{
    public class MarketOrderInfo
    {
        public bool IsBuyOrder { get; set; }

        [DisplayName("Price")]
        [DisplayFormat(DataFormatString = "{0:N2}", NullDisplayText = "-")]
        public double Price { get; set; }
        
        [DisplayName("Normed Price")]
        [DisplayFormat(DataFormatString = "{0:N2}", NullDisplayText = "-")]
        public double NormedPrice { get; set; }

        [DisplayName("Solar System")]
        public SolarSystem SolarSystem { get; set; }
        
        [DisplayName("Broker Fee")]
        [DisplayFormat(DataFormatString = "{0:N2}", NullDisplayText = "-")]
        public double BrokerFee { get; set; }
        
        [DisplayName("Transaction Tax")]
        [DisplayFormat(DataFormatString = "{0:N2}", NullDisplayText = "-")]
        public double TransactionTax { get; set; }
        
        [DisplayName("Estimated Revenue (Gross)")]
        [DisplayFormat(DataFormatString = "{0:N2}", NullDisplayText = "-")]
        public double EstimatedRevenueGross { get; set; }
        
        [DisplayName("Estimated Revenue (Net)")]
        [DisplayFormat(DataFormatString = "{0:N2}", NullDisplayText = "-")]
        public double EstimatedRevenueNet => EstimatedRevenueGross - BrokerFee - TransactionTax;
    }
}