using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NebularMining.Core;

namespace NebularMining.WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MarketController : ControllerBase
    {
        private static string[] _oreList = new string[] {
            "Veldspar",
            "Concentrated Veldspar",
            "Compressed Veldspar",
            "Stable Veldspar",
            "Dense Veldspar",
            "Scordite",
            "Condensed Scordite",
            "Massive Scordite",
            "Plagioclase",
            "Azure Plagioclase",
            "Rich Plagioclase",
            "Pyroxeres",
            "Solid Pyroxeres",
        };

        private MarketAnalyzer _marketAnalyzer;

        public MarketController(MarketAnalyzer marketAnalyzer)
        {
            _marketAnalyzer = marketAnalyzer;
        }

        [HttpGet]
        public async Task<IEnumerable<MarketStats>> Get([FromQuery(Name = "region")] ulong region)
        {
            return await _marketAnalyzer.GetStats(region, _oreList);
        }
    }
}