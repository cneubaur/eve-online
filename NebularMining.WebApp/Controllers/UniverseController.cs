using System.Collections.Generic;
using System.Threading.Tasks;
using EveOnline.Client;
using Microsoft.AspNetCore.Mvc;
using NebularMining.Core;

namespace NebularMining.WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UniverseController : ControllerBase
    {
        private UniverseClient _universeClient;

        private IEnumerable<NameInfo> _regions;

        public UniverseController(UniverseClient universeClient)
        {
            _universeClient = universeClient;
        }

        [HttpGet("regions")]
        public async Task<IEnumerable<NameInfo>> Get()
        {
            if (_regions is null)
            {
                var regionIds = await _universeClient.List<Region>();
                _regions = await _universeClient.GetNameInfo(regionIds);
            }

            return _regions;
        }
    }
}