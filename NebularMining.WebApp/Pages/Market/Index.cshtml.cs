using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EveOnline.Client;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using NebularMining.Core;

namespace NebularMining.WebApp
{
    public class IndexModel : PageModel
    {
        private UniverseClient _universeClient;

        public IEnumerable<NameInfo> Regions { get; private set; }

        public IndexModel(UniverseClient universeClient)
        {
            _universeClient = universeClient;
        }

        public async Task OnGet()
        {
            var regionIds = await _universeClient.List<Region>();
            Regions = (await _universeClient.GetNameInfo(regionIds)).OrderBy(x => x.Id);
        }
    }
}
