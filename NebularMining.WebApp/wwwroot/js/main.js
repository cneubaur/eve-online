﻿Vue.filter('float', value => {
  return new Intl.NumberFormat('de-DE', { minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(value);
});

Vue.filter('integer', value => {
  return new Intl.NumberFormat('de-DE').format(value);
});

let app = new Vue({
  el: '#app',
  data: {
    regions: [],
    regionSearch: null,
    marketStats: [],
    selectedRegion: null,
    loadedRegion: null,
    isLoadingStats: false,
  },
  computed: {
    filteredRegions() {
      let filtered = this.regions;

      if (this.regionSearch !== null && this.regionSearch !== '') {
        filtered = this.regions.filter(region => {
          return region.name.toLowerCase().indexOf(this.regionSearch.toLowerCase()) > -1;
        });
      }

      if (filtered.length > 10) {
        return filtered.slice(0, 11);
      }

      return filtered;
    }
  },
  created() {
    axios.get('/api/universe/regions')
      .then(response => {
        this.regions = response.data;
      });
  },
  methods: {
    fetchStats(region) {
      if (this.selectedRegion === null || this.isLoadingStats) {
        return;
      }

      this.isLoadingStats = true;

      axios.get('/api/market', {
          params: {
            region: region.id,
          }
        })
        .then(response => {
          this.marketStats = response.data;        
          this.loadedRegion = region;
        })
        .catch(error => {

        })
        .then(error => {
          this.isLoadingStats = false;
        });
    }
  }
});