﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using EveOnline.Client;
using NebularMining.Core;

namespace NebularMining.Commandline
{
    class Program
    {
        static string[] _oreList = new string[] {
            "Veldspar",
            "Concentrated Veldspar",
            "Compressed Veldspar",
            "Scordite",
            "Condensed Scordite",
            "Massive Scordite",
            "Plagioclase",
            "Azure Plagioclase",
            "Rich Plagioclase",
            "Pyroxeres",
        };

        const string _sdePath = @"..\Resources\sde";
        const string _jsonDataPath = @".\Data";

        static void Main(string[] args)
        {
            var services = new ServiceCollection();

            services.AddLogging(builder => {
                builder.AddFilter("Microsoft", LogLevel.Warning);
                builder.AddFilter("System", LogLevel.Warning);
                builder.ClearProviders();
                builder.AddConsole();
            });

            services.AddHttpClient<UniverseClient>(client => {
                client.BaseAddress = new Uri("https://esi.evetech.net/latest/");
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "");
            });

            services.AddHttpClient<MarketClient>(client => {
                client.BaseAddress = new Uri("https://esi.evetech.net/latest/");
            });

            services.AddScoped<MarketAnalyzer>();

            var serviceProvider = services.BuildServiceProvider();

            using var scope = serviceProvider.CreateScope();

            var marketAnalyzer = scope.ServiceProvider.GetRequiredService<MarketAnalyzer>();
            var universeClient = scope.ServiceProvider.GetRequiredService<UniverseClient>();

            var regionId = universeClient.Search<Region>("Metropolis").Result.First();
            Console.WriteLine(regionId);

            var marketStats = marketAnalyzer.GetStats(regionId, _oreList, securityStatus: 0.0).Result;

            // var output = marketStats.AsTable(
            //     ("Item", x => x.ItemType.Name),
            //     ("Volume", x => $"{x.ItemType.Volume:N2} m3"),
            //     ("Lowest Price", x => $"{x.LowestPrice:N2} ISK"),
            //     ("Normed Price", x => $"{x.NormedLowestPrice:N2} ISK"),
            //     ("Highest Price", x => $"{x.HighestPrice:N2} ISK"),
            //     ("Average Sold Volume", x => $"{x.AverageSoldVolume:N2} m3"),
            //     ("Average Sold Units", x => $"{x.AverageSoldUnits:N0}"),
            //     ("Estimated Revenue", x => $"{x.EstimatedRevenueNet:N2} ISK")
            // );

            // Console.WriteLine(output);
        }
    }

    static class EnumerableExtensions
    {
        public static string AsTable<T>(this IEnumerable<T> items, params (string, Func<T, string>)[] columns)
        {
            var formats = columns.Select(x => x.Item2);
            var grid = items.Select(x => formats.Select(map => map(x)).ToArray());
            
            var headers = columns.Select(x => x.Item1).ToArray();

            var colWidths = columns.Select((x, i) => {
                var lengths = grid.Select(x => x[i].Length).ToList();
                lengths.Add(headers[i].Length);
                return lengths.Max();
            });

            var format = string.Join("  ", colWidths.Select((x, i) => $"{{{i},{x}}}"));

            var output = "";
            output += string.Format(format, headers.ToArray());
            output += "\n" + string.Format(format, colWidths.Select(x => String.Concat(Enumerable.Repeat("-", x))).ToArray());
            output += "\n" + string.Join("\n", grid.Select(x => string.Format(format, x.ToArray())));

            return output;
        }
    }
}
